﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace os2final
{
    public partial class Form1 : Form
    {
        public static Queue queue = new Queue();
        public static Queue queue2 = new Queue();
        public static object deadlockavoid = new object(); //to avoid dead lock that can happen when (while a meal is in its way to the queu(because all chefs are busy ) and all the chefs end their work before the meal enqueued so they will wait for another meal to wakeup one of them to work     
        //key for each chef
         static Chef[] chefs;

        public static object queuelock;  //to avoid making 2 chefs cook the same meal(to lock the queue while dequeueing)
        public static object update;
        public static object update2;

        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            queuelock = new object();
            update = new object();
            update2 = new object();
            int nochef = 3;
            chefs = new Chef[nochef];
            chefs[0] = new Chef(this.label1,this.label4,this.label5);
            chefs[1] = new Chef(this.label2, this.label4, this.label5);
            chefs[2] = new Chef(this.label3, this.label4, this.label5);

            for (int count = 0; count < nochef; count++)
            {
                //chefs[count]=new Chef();
                chefs[count].setname(count.ToString());
                chefs[count].setBusy(false);
            }
        
        
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private  void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            int n = 0;
            try
            {
                n = Int32.Parse(this.textBox1.Text);
            }
            catch (Exception ex)
            {

                Console.Out.WriteLine("error parsing the number" + ex);
            }
            Random rand = new Random();
            Task[] meals = new Task[n];

            await Task.Run(() => {
                for (int i = 0; i < n; i++)
                {  //create array of meals and each meal contains its own thread 
                    int time = rand.Next(6000, 7000);
                    Console.Out.WriteLine("meal " + i + "created with time : " + time);
                    meal m = new meal(time); //create meal and give it random time value
                    // m.Name = i;
                    m.setname(i.ToString());
                    meals[i] = new Task(() =>
                    {
                        Thread.Sleep(rand.Next(500, 3000));
                        Console.Out.WriteLine("meal " + m.getname() + " start runing");
                        // Boolean m.enrolled = false; //to stop when finding free chef
                        for (int chef = 0; chef < 3; chef++) //3 is the number of chefs
                        {
                            if (!m.enrolled)
                            {
                                lock (chefs[chef].key) //critical section
                                {
                                    //if (chefs[chef].isbusy())
                                    //    Console.Out.WriteLine("meal " + m.getname() + " checked for chef " + chef + " but he is busy ");
                                    if (!chefs[chef].isbusy())
                                    {
                                        // this.label1("chef " + chef + " waked up by meal " + m.getname());
                                        //chefs[chef].setBusy(true);
                                        m.enrolled = true;
                                        m.WakeUpchef(chefs[chef]);
                                    }
                                }
                            }
                        }
                        if (!m.enrolled) //check if there is no free chef so go to queue
                        {
                            Console.Out.WriteLine("meal " + m.getname() + " was enqueed ");
                            lock (deadlockavoid)
                            {
                                queue.Enqueue(m);

                            }
                            lock (Form1.update){
                            Chef.updatequeue(this.label4);

                             }
                        }


                    });


                }




              
            
            
            }
            
            
            
            
            );


            Task.Run(() => {
                for (int counter = 0; counter < n; counter++)  //run the  threads
                {
                    meals[counter].Start();
                    //Thread.Sleep(rand.Next(1000,1500));
                } 
            
            }); 
            //Thread.Sleep(2000);
              
       


          //  Console.Out.WriteLine("size = " + queue.Count);

            //Console.In.ReadLine();




        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
