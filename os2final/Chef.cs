﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace os2final
{
    class Chef
    {
        private Label statue;
        private Label update;
        private Label update2;
        public Chef(Label l,Label u,Label u2)
        {
            this.statue = new System.Windows.Forms.Label();
            workson = null;
            this.key = new object();
            this.busy = false;
            this.statue = l;
            this.update = u;
            this.update2 = u2;
        }
        public void setstatue(String s)
        {
            this.statue.Text += " \n "+ s;
        }

        public static void updatequeue(Label qustatue)
        {
            String s = "";
           
            if (Form1.queue.Count != 0)
            {
                meal[] meals = new meal[Form1.queue.Count];
                Form1.queue.CopyTo(meals,0);
            for (int i = 0; i < meals.Count(); i++)
            {
               // meal m = (meal)Form1.queue.Dequeue();
                
                s += " meal " + meals[i].name + " \n ";
            }
           
        }
            qustatue.Text = s;
        }
        public static void updatequeue2(Label qustatue)
        {
            String s = "";
           
            if (Form1.queue2.Count != 0)
            {
                meal[] meals = new meal[Form1.queue2.Count];
                Form1.queue2.CopyTo(meals, 0);
                for (int i = 0; i < meals.Count(); i++)
                {
                    // meal m = (meal)Form1.queue.Dequeue();

                    s += " meal " + meals[i].name + " \n ";
                }
                qustatue.Text = s;
            }
        }







        private String name;
        public void setname(String n)
        {
            name = n;
        }
        public String getname()
        {
            return name;
        }
        private Boolean busy;
        public object key;
        private meal workson;
        public void setworkon(meal a)
        {
            this.workson = a;
        }
        public Boolean isbusy()
        {
            return busy;
        }

        public void setBusy(Boolean v)
        {
            this.busy = v;
        }
        public async void cook()
        {
            //this.busy = true;
            if (this.workson != null) //then a meal called the function
            {
                await Task.Run(() =>
                {
                    Thread.Sleep(this.workson.time);
                }
                    );
                //Console.Out.WriteLine("meal : " + this.workson.name + " finished by : chef " + this.name);
                this.setstatue("meal : " + this.workson.name + " finished ");
                Form1.queue2.Enqueue(this.workson);
                lock (Form1.update2)
                {
                    Chef.updatequeue2(this.update2);
                }
                    
                this.workson = null;
            }
            else //check for the queue
            {
                lock (Form1.queuelock)
                {
                    //critical section only one chef can remove the meal to cook it
                    if (Form1.queue.Count != 0)
                    {
                        this.workson = (meal)Form1.queue.Dequeue();
                    }

                }
                if (this.workson != null)
                {
                    //Console.Out.WriteLine(" chef " + this.name + " dequed meal "+ this.workson.getname()+" to be cooked");
                    this.setstatue(" dequed meal " + this.workson.getname() + " to be cooked");

                    //cook a meal 

                    //  this.workson.t.Start();
                    await Task.Run(() =>
                    {
                        Thread.Sleep(this.workson.time);
                    }
                     );
                    // Console.Out.WriteLine("meal : " + this.workson.getname() + " finished by : chef " +this.name );
                    this.setstatue("meal : " + this.workson.getname() + " finished");
                    Form1.queue2.Enqueue(this.workson);
                    lock (Form1.update)
                    {
                        Chef.updatequeue(this.update);
                    }
                    lock (Form1.update2)
                    {
                        Chef.updatequeue2(this.update2);
                    }
                    
                    this.workson = null;
                }
            }
            lock (Form1.deadlockavoid)
            {
                //   this.workson = null;
                if (Form1.queue.Count == 0)
                {
                    //this.workson = null;
                    this.busy = false;
                    //Console.Out.WriteLine(" chef " + this.name + " is free");
                    this.setstatue(" chef " + this.name + " is free");
                }
                else
                {
                    this.cook();
                }
            }

        }





    }
}
